type LLNode<T> = {
    value: T,
    next: LLNode<T> | null
}

const number_node: LLNode<number> = {
    value: 1,
    next: null
}

const string_node: LLNode<string> = {
    value: "Hello",
    next: null
}

type User = {
    name: string,
    age: number
}

const user_node: LLNode<User> = {
    value: {
        name: "John",
        age: 30
    },
    next: null
}


