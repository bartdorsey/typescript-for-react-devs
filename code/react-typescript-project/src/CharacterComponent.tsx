import type { Character } from './App';

type CharacterComponentProps = {
    character: Character
}

export default function CharacterComponent({ character }: CharacterComponentProps) {
    return (
       
      <h1>{character.name}</h1>
    )    
}
