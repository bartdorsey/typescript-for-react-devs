import { useEffect, useState } from "react"
import z from "zod"
import "./App.css"
import CharacterComponent from "./Character"

const BASE_URL = "https://swapi.dev/api/"

const CharacterSchema = z.object({
    name: z.string(),
    height: z.string(),
    hair_color: z.string(),
    films: z.array(z.string()),
})

export type Character = z.infer<typeof CharacterSchema>

type CharacterForm = {
    characterId: { value: string }
} & EventTarget

async function getCharacterById(id: string): Promise<Character | Error> {
    const response = await fetch(`${BASE_URL}/people/${id}`)

    if (!response.ok) {
        return Error("Failed to fetch character")
    }
    const data = await response.json()
    const parsedData = CharacterSchema.safeParse(data)
    if (!parsedData.success) {
        return Error(parsedData.error.message)
    }

    return parsedData.data
}

const defaultCharacterId = "1"

function App() {
    const [character, setCharacter] = useState<Character>()
    const [error, setError] = useState<Error>()

    async function handleSubmit(e: React.SyntheticEvent) {
        e.preventDefault()
        const form = e.target as CharacterForm
        setError(undefined)
        const character = await getCharacterById(form.characterId.value)

        if (character instanceof Error) {
            setError(character)
            return
        }
        setCharacter(character)
    }

    useEffect(() => {
        getCharacterById("1").then((character) => {
            if (character instanceof Error) {
                setError(character)
                return
            }
            setCharacter(character)
        })
    }, [])

    if (!character) {
        return <div>Loading...</div>
    }

    return (
        <main>
            <div>{error && error.message}</div>
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    name="characterId"
                    defaultValue={defaultCharacterId}
                />
                <button>Fetch Character</button>
            </form>
            <CharacterComponent character={character} />
        </main>
    )
}

export default App
